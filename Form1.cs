﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RDotNet;
using R;

namespace R.Net
{
    public partial class Form1 :Form
    {
        public Form1() {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e) {
            REngine engine = REngine.GetInstance();
            engine.Initialize();
            engine.Evaluate("library(ggplot2movies)");
            engine.Evaluate("mov = na.omit(ggplot2movies::movies)");
            engine.Evaluate("t_test <- t.test(mov[which(mov$year < 1996),]$rating, mov[which(mov$year > 1996),]$rating)");
            double t = Math.Round(engine.Evaluate("t_test$statistic").AsNumeric()[0], 3);
            double p = Math.Round(engine.Evaluate("t_test$p.value").AsNumeric()[0], 3);
            textBox1.Text = t.ToString();
            textBox2.Text = p.ToString();
        }
    }
}
